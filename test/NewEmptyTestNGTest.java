/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.testng.Assert;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 *
 * @author Mohammed Fareed
 */
public class NewEmptyTestNGTest {
    
    public NewEmptyTestNGTest() {
    }
    @DataProvider(name = "userNamesPass")
    public Object[][] verifyUserAndPass(){
        return new Object[][]{
                {true, "fared", "fared"},
                {false, "fared", "f"}
        };
    }
    @Test(dataProvider = "userNamesPass")
    public void verifyLogInInfoByUserName(Boolean expected, String name, String pass){
        Assert.assertEquals(expected, Systems.verifyLogInInfoByUserName(name, pass));
    }
    ///////////////////////////
    @DataProvider(name = "userNames")
    public Object[][] verifyUser(){
        return new Object[][]{
                {false, "fared"},
                {true, "f"}
        };
    }
    @Test(dataProvider = "userNames")
    public void validateUserName(boolean expected, String name){
        Assert.assertEquals(expected, UserController.validateUserName(name));
    }
    
    
//    @DataProvider(name = "id")
//    public Object[][] verifyLoadListOfUsers(){
//        return new Object[][]{
//                {null, -1},
//                {game.getGameDetails(), 1}
//        };
//    }
    @Test
    public void loadListOfUsers(){
        UserDB.loadListOfUsers();
        if (UserDB.getListOfUsers().isEmpty())
            System.out.println("The list is empty");
        else
            System.out.println("The list is not empty");
    }
//    String name = "fared";
//    @Test
//    public void validateUserName(){
//        Assert.assertEquals(expected, UserController.validateUserName(name));
//    }
    

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
