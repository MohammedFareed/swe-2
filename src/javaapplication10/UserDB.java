
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Vector;

public class UserDB {
	private static Vector<User> listOfUsers = new Vector<User>();
	// carries students and teachers identified by attribute type

	static void addNewUser(User user) {
		System.out.println("YES" + user.getAccount());
		listOfUsers.add(user);
	}

	public static Vector<User> getListOfUsers() {
            loadListOfUsers();
            return listOfUsers;
	}

	static void updatePassword(String userName, String newPass) {

	}

	static void updateEmail(String userName, String newPass) {

	}

	static void updateUserPoints(int value) {

	}

	static void updateTeacherDomains(Vector<Domain> domains) {

	}

	public static void saveListOfUsers() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("users.txt"));
			oos.writeObject(listOfUsers);
			oos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	
	}

	public static void loadListOfUsers() {
		try {
                    File f = new File("users.txt");
                    if (f.length() != 0) {
                            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("users.txt"));
                            listOfUsers = (Vector<User>) (ois.readObject());
                            ois.close();
                }
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
}
