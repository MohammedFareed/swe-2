import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class GameDB {

	static ArrayList<Game> listOfGames = new ArrayList<Game>();

	static Game loadGame(int gameID) {
            loadListOfGames();
            for (int i = 0; i < listOfGames.size(); i++) {
                    if (listOfGames.get(i).getGameDetails().getGameID() == gameID)
                            return listOfGames.get(i);
            }
            return null;
	}

	static ArrayList<GameDetails> loadGamesDetails() {
		ArrayList<GameDetails> GD = new ArrayList<GameDetails>();

		for (int i = 0; i < listOfGames.size(); i++) {
			GD.add(listOfGames.get(i).getGameDetails());
		}
		return GD;

	}

	static void updateGameRating(int gameID, double rate) {
		// DB
	}

	static void deleteGame(Game game) {
		listOfGames.remove(game);
	}

	static void addGame(Game game) {
		listOfGames.add(game);
	}

	public static void saveListOfGames() {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("games.txt"));
			oos.writeObject(listOfGames);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static void loadListOfGames() {
		try {
			File f = new File("games.txt");
			if (f.length() != 0) {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream("games.txt"));
				listOfGames = (ArrayList<Game>) ois.readObject();
				Systems.updateGamesFromDB();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();

		}
	}

}
