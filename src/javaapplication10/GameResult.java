import java.io.Serializable;

public class GameResult  implements Serializable{

	private int numOfUsedHints;
	private int numOfRightAnswers;
	private int numOfQuestions;
	private double dynamicScore;

	void updateNumOfHints() {
		numOfUsedHints++;
	}

	void calculateScore(double score) {
		dynamicScore = score * (numOfRightAnswers / (numOfQuestions * 1.0));
		dynamicScore = Math.max(dynamicScore - (numOfUsedHints * 10), 0);
	}

	double getdynamicScore() {
		return dynamicScore;
	}

	void addToAnswers(String rightAnswer, String userAnswer) {
		numOfQuestions++;
		if (userAnswer.equals(rightAnswer))
			numOfRightAnswers++;
	}

	void clear() {
		numOfUsedHints = 0;
		numOfRightAnswers = 0;
		numOfQuestions = 0;
		dynamicScore = 0.0;
	}
}
