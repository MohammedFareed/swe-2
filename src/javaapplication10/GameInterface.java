public class GameInterface {

	static void runGame(Game game) {
		GameResult GR = game.getGameResult();
		GR.clear();

		for (int i = 0; i < game.getGameDetails().getNumOfQuestions(); i++) {
			System.out.println("\n\n" + game.getGameQuestions().get(i).getQuestion() + "\n");

			String[] choices = game.getGameQuestions().get(i).getChoices();

			for (int j = 0; j < (game.getGameDetails().getType().equals("MCQ") ? 4 : 2); j++) {
				System.out.println((j + 1) + ". " + choices[j]);
			}
			System.out.println("1.Answer\n2.Hint");

			if(UserInterface.cin.nextInt() == 2)
			System.out.println(game.getGameQuestions().get(i).getHint());

			System.out.print("Answer number: ");
			GR.addToAnswers(game.getGameQuestions().get(i).getRightAnswer(), choices[UserInterface.cin.nextInt() - 1]);
		}

	}

	static void QuestionInfo(Question question, Boolean MCQ) {
		System.out.print("\n\nQuestion: ");
		question.setQuestion(UserInterface.cin.next());
		System.out.print("The Right Answer: ");
		question.setRightAnswer(UserInterface.cin.next());
		System.out.print("Hint: ");
		question.setHint(UserInterface.cin.next());

		if (MCQ) {
			String[] answers = new String[4];
			System.out.println("Enter 4 answers: ");

			for (int i = 0; i < 4; i++) {
				System.out.print((i + 1) + "). ");
				answers[i] = UserInterface.cin.next();
			}

			question.setChoices(answers);
		}

	}

	static void editGame(Game game) {
		int check = 0;
		System.out.println(game.getGameDetails());

		while (check != 2) {
			for (int i = 0; i < game.getGameQuestions().size(); i++)
				System.out.println(i + ". " + game.getGameQuestions().get(i).getQuestion());

			System.out.println("Number of Question you want to edit");
			int index = UserInterface.cin.nextInt();
			UserInterface.cin.nextLine();

			game.addQuestion();

			game.getGameQuestions().set(index, game.getGameQuestions().get(game.getGameQuestions().size() - 1));
			game.getGameQuestions().remove(game.getGameQuestions().size() - 1);

			System.out.println("1.Edit more question enter\n2.Done");
			check = UserInterface.cin.nextInt();
		}
	}

	static void viewResult(double score) {
		String result = "";

		if (score > 0.0)
			result += "Your Score " + score + "\nCongratulations! You Win";
		else
			result += "Sorry You Lost, Try Again..";

		System.out.println(result);

	}

	static void fillGameDetails(GameDetails gameDetails) {
		System.out.print("\nGame Name: ");
		gameDetails.setName(UserInterface.cin.next());
		System.out.print("Score: ");
		gameDetails.setScore(UserInterface.cin.nextDouble());
		System.out.print("Description: ");
		gameDetails.setDescription(UserInterface.cin.next());
		System.out.print("Resources: ");
		gameDetails.setResource(UserInterface.cin.next());

		System.out.println("\n1." + Domain.Math + "\n2." + Domain.Science);
		System.out.println("3." + Domain.Chemistry + "\n4." + Domain.Programming);
		System.out.print("Choose Domain Number: ");
		gameDetails.setDomain(UserInterface.cin.nextInt());

		System.out.println("\n1.MCQ\n2.T/F\nChoose Type: ");
		if (UserInterface.cin.nextInt() == 1)
			gameDetails.setType("MCQ");
		else
			gameDetails.setType("T/F");

		System.out.print("Number Of Questions: ");
		gameDetails.setNumOfQuestions(UserInterface.cin.nextInt());
		UserInterface.cin.nextLine();
	}

}
