
import java.io.Serializable;
import java.util.Vector;

public class Teacher extends User{

	private Vector<Domain> domains = new Vector<Domain>();

	public void setDomain(Vector<Domain>d) {
		domains = d;
	}
	public Vector<Domain> getDomain() {
		return domains;
	}

	public String getType() {
		return "teacher";
	}
	public String showProfile() {
		String result = "Domains:\n";
		for (int i = 0; i < domains.size(); i++) {
			result += ((i + 1) + ") " + domains.get(i).toString()) + "\n";
		}
		return super.showProfile() + "\n" + result;
	}

}