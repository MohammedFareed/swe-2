import java.io.Serializable;

public class Account implements Serializable {
	private String userName, password, email;
	Date birthdate;
	private Date dateOfRegistration;
	private Gender gender;


	public String toString() {
		String acc = "username: " + userName + "\n" + "password: " + password + "\n" + "email: " + email + "\n"
				+ "birthdate: " + birthdate + "\n" + "date of registeration " + dateOfRegistration + "\n" + "gender: "
				+ gender;
		return acc;
	}

	public void setUserName(String name) {
		userName = name;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public void setGender(int type) {
		if (type == 1)
			gender = Gender.Male;
		else 
			gender = Gender.Female;
	}

	public void setdateOfRegistration(Date dor) {
		dateOfRegistration = dor;
	}

	public void setbirthdate(Date bd) {
		birthdate = bd;
	}

	public Date getbirthdate() {
		return birthdate;
	}

	public Date getdateOfRegistration() {
		return dateOfRegistration;
	}

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}
	
	public Gender getGender() {
		return gender;
	}

	public String getEmail() {
		return email;
	}

}
