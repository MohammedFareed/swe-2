public class GameController {
	
	public static void establishGame( int gameID )
	{
		Game game = GameDB.loadGame(gameID);
		GameInterface.runGame(game);
		boolean firstScore = game.addPlayer(Systems.getLoggedInUser().getUserID());
		UserController.addNewScore(firstScore , gameID , game.getResults() ) ;
	}
	
	public static void newGame(Teacher teacher)
	{
		Game newGame = new Game();
		newGame.createGame(teacher);
		GameDB.addGame(newGame);
		Systems.updateGamesFromDB();

	}
	
	void updateGameRating()
	{
		//DB
	}
	
}
