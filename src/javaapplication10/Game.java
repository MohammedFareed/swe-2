import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

public class Game  implements Serializable{
	
	private GameDetails gameDetails ; 
	private GameResult gameResult;
	private static ArrayList<Integer> playersIDs = new ArrayList<Integer>() ;
	private ArrayList<Question> questions = new ArrayList<Question>() ;	
 
	Game() {
		gameDetails = new GameDetails() ;
		gameResult = new GameResult() ;
	}
	
	public void createGame(Teacher teacher)
	{
		gameDetails.setCreator(teacher);
		GameInterface.fillGameDetails(gameDetails) ;
		
		for(int i=0 ; i<gameDetails.getNumOfQuestions() ; i++)
		{
			this.addQuestion() ;
		}		
	}
	
	public void addQuestion()
	{
		Question question ;
		
		if(gameDetails.getType().equals("MCQ"))
			question = new MCQ() ;
		else
			question = new TrueAndFalse() ;
		
			GameInterface.QuestionInfo(question , (gameDetails.getType().equals("MCQ") ? true : false ) ) ;
			
		questions.add(question) ;		
	}
	
	double getResults()
	{
		gameResult.calculateScore(gameDetails.getScore());
				
		if(gameResult.getdynamicScore()/(gameDetails.getScore()*1.0) >= 0.5 )
		{
			GameInterface.viewResult(gameResult.getdynamicScore());
			return gameResult.getdynamicScore() ;
		}
		else
			GameInterface.viewResult(0.0);
		
		return 0.0 ;
	}
	
	public GameDetails getGameDetails() {
		return gameDetails;
	}
	
	public ArrayList<Question> getGameQuestions() {
		return questions;
	}
	
	public GameResult getGameResult()
	{
		return gameResult ;
	}
	
	public static boolean addPlayer(int userID) {
		for(int i=0 ; i<playersIDs.size() ; i++)
		{
			if(playersIDs.get(i) == userID )
				return false;
		}
		playersIDs.add(userID) ;
		return true ;
	}
}
