import java.util.Vector;

public class UserController {

	public static boolean validateUserName(String name) { // false if this name is taken
		for (int i = 0; i < UserDB.getListOfUsers().size(); i++) {
			if (UserDB.getListOfUsers().get(i).getAccount().getUserName().equals(name)) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean createNewUser(int type, Vector<Domain>domains)
	{ // 1 student 2 teacher
		
		Account account = new Account();
		boolean valid = UserInterface.getAccountInfo(account);
		if (!valid)
			return false;

		User user;
		if (type == 1) 
			user = new User();
		else {
			user = new Teacher();
			user.setDomain(domains);
		}
			
	
		user.setAccount(account);
		Systems.addUser(user);
		Systems.setLoggedInUser(user);
		return true;
	}

	public static void addNewScore(boolean firstScore , int gameID , double newScore)
	{
		if( firstScore )
		{
			Systems.getLoggedInUser().updateScore(newScore);
			if(newScore > 0.0)
			Systems.getLoggedInUser().addScoreGame(gameID , newScore) ;
		}
		else
		{
			double oldScore = Systems.getLoggedInUser().getScoreGame(gameID) ;
			
			Systems.getLoggedInUser().updateScore(Math.max(newScore,oldScore)-oldScore);
			Systems.getLoggedInUser().addScoreGame(gameID , Math.max(newScore,oldScore)-oldScore) ;

		}
	}
}
