import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

public class User implements Serializable{
	private int userID;
	private double score;
	private Account account;
	private Map<Integer,Double> gameScore = new HashMap<Integer,Double>() ; 
	private AtomicInteger count = new AtomicInteger(0);
	private boolean isTerminated;

	User() {
		isTerminated = false;
		score = 0.0;
		userID = count.incrementAndGet();
	}

	public String getType() {
		return "student";
	}

	public void setAccount(Account a) {
		account = a;
	}
	public void setState(boolean state) {
		isTerminated = state;
	}
	
	public boolean getState() {
		return isTerminated;
	}

	public void updateScore(double score) {
		this.score += score;
	}

	public double getScore() {
		return score;
	}

	public int getUserID() {
		return userID;
	}

	public Account getAccount() {
		return account;
	}
	public void setDomain(Vector<Domain>d) {
	}
	public Vector<Domain> getDomain() {
		return null;
	}
	public String showProfile() {
		return (account + "\nscore: " + score);
	}
	
	void addScoreGame(int gameID , double score)
	{
		gameScore.put(gameID, score) ;
	}
	
	double getScoreGame(int gameID)
	{
		return gameScore.get(gameID) ;
	}

}
